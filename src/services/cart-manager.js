export class CartManagerService {

  static _instance;

  /**
   * Unique service instance
   * @return {CartManagerService}
   */
  static get instance() {
    if (CartManagerService._instance == undefined) {
      CartManagerService._instance = new CartManagerService();
    }

    return CartManagerService._instance;
  }

  _cart = {
    totalPrice: 0,
    books: {}
  };

  get cart() {
    return this._cart;
  }

  /**
   * Adds a book to the cart.
   * @param {number} id 
   * @param {number} price 
   */
  add(id, price) {
    const book = this.cart.books[id];

    if (book) {
      book.quantity++;
    } else {
      this.cart.books[id] = {
        price,
        quantity: 1
      };
    }

    this._calculateTotalPrice();
  }

  /**
   * Removes a book from the cart.
   * @param {number} id 
   */
  remove(id) {
    const book = this.cart.books[id];

    book.quantity --;

    if (book.quantity <= 0) {
        delete this.cart.books[id];
    }

    this._calculateTotalPrice();
  }

  _calculateTotalPrice() {
    let totalPrice = 0;

    Object.keys(this.cart.books).forEach(id => {
        const book = this.cart.books[id];

        totalPrice += book.price * book.quantity;
    });

    this.cart.totalPrice = totalPrice;
  }
}
