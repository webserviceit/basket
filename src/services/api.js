import * as zmq from 'zeromq';
import axios from 'axios';
import { config as envConfig } from 'dotenv';

const config = envConfig();

export class ApiService {

  static _instance;

  static _API_URL = config['API_URL'];

  static _BUS_URL = config['BUS_URL'];

  /**
   * Unique service instance
   * @return {ApiService}
   */
  static get instance() {
    if (ApiService._instance == undefined) {
      ApiService._instance = new ApiService();
    }

    return ApiService._instance;
  }

  /**
   * Finds a book by its ID.
   * @param {number} id 
   * @throws exception if not found.
   */
  async getBook(id) {
    console.log(`Get book on ${ApiService._API_URL}`);
    const response = await axios.get(ApiService._API_URL + '/livres/' + id);

    return response.data;
  }

  /**
   * Mark a book as saved.
   * @param {number} id 
   * @throws exception when cannot be saved.
   */
  async saveBook(id) {
    return await this._sendMessage({
      action: 'saveBook',
      id
    });
  }

  /**
   * Mark a book as free.
   * @param {number} id
   * @throws exception when cannot be freed.
   */
  async freeBook(id) {
    return await this._sendMessage({
      action: 'freeBook',
      id
    });
  }

  /**
   * Says a message to the bus API. 
   * @param {object} payload
   * @return {boolean} 
   */
  async _sendMessage(payload) {
    try {
      const request = new zmq.Request({ sendTimeout: 10 });
      request.connect(ApiService._BUS_URL);

      console.log('Send', payload);
  
      await request.send(JSON.stringify(payload));
      const [result] = await request.receive();
  
      return result.toString() === "true";
    } catch (e) {
      console.log('Bus error');
      return false;
    }
  }
}
