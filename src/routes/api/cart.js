import express from 'express';
import asyncHandler from 'express-async-handler';
import { ApiService } from '../../services/api';
import { CartManagerService } from '../../services/cart-manager';

export const cartRouter = express.Router();

// Get all books
cartRouter.get('/', asyncHandler(
    async (req, res) => res.json(CartManagerService.instance.cart))
);

cartRouter.post('/add/:id', asyncHandler(
    async (req, res) => {
        const { id } = req.params;

        try {
            const book = await ApiService.instance.getBook(id);
            const addedWithSuccess = await ApiService.instance.saveBook(id);

            if (addedWithSuccess) {
                CartManagerService.instance.add(book['id'], book['Prix']);

                res.json(CartManagerService.instance.cart);
            } else {
                res.json({ error: 'Failed to save book.' });
            }
        } catch (e) {
            res.json({ error: 'Unknown book.' });
        }
    })
);

cartRouter.post('/remove/:id', asyncHandler(
    async (req, res) => {
        const { id } = req.params;

        if (CartManagerService.instance.cart.books[id]) {
            const removedWithSuccess = await ApiService.instance.freeBook(id);
    
            if (removedWithSuccess) {
                CartManagerService.instance.remove(id);
            }
        }

        res.json(CartManagerService.instance.cart);
    })
);
