import express from 'express';
import { cartRouter } from './cart';

export const apiRouter = express.Router();
apiRouter.use('/cart', cartRouter);
