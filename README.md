# Setup

```
npm install
npm run start
```

# Endpoints

**See the current cart:**
```
GET
localhost:3000/api/cart
```

**Add a book to the cart:**
```
GET
localhost:3000/api/cart/add/:id
```

A book can be added multiple time (to change the quantity).
To do so, execute the request multiple times.

**Remove a book to the cart:**

```
GET
localhost:3000/api/cart/remove/:id
```
